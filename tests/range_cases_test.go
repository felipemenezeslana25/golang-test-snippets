package tests

import (
	"fmt"
	"testing"
)

// Godocs Exemplo - Testing https://golang.org/doc/code.html#Testing

func TestReverseRunes(t *testing.T) {
	cases := []struct {
		one, two string
	}{
		{"Um", "Dois"},
		{"One", "Two"},
		{"1", "2"},
	}
	for _, c := range cases {
		fmt.Printf("One: %s, Two: %s\n", c.one, c.two)
	}
}