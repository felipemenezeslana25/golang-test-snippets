package tests

import "testing"

// Roda todos os testes de CRUD
func TestCRUD(t *testing.T) {
	t.Run("Create", func(t *testing.T) { testCreate(t, srv) })
	t.Run("Read", func(t *testing.T) { testRead(t, srv) })
	t.Run("Update", func(t *testing.T) { testUpdate(t, srv) })
	t.Run("Delete", func(t *testing.T) { testDelete(t, srv) })
}